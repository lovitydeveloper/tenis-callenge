const app = require('./app');
const { sequelize } = require('./config/database');

const port = process.env.PORT || 3000;

// Conexión a la base de datos y sincronización de los modelos
//db.testConnection();

sequelize.sync()
  .then(() => {
    console.log('Database connected');
    // Iniciar el servidor
    app.listen(port, () => {
      console.log(`Server listening on port ${port}`);
    });
  })
  .catch((error) => {
    console.error('Unable to connect to the database:', error);
  });
