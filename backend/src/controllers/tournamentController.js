const { Tournament } = require('../models');

// Obtener todos los torneos
async function getAllTournaments(req, res) {
  try {
    const tournaments = await Tournament.findAll();
    res.status(200).json(tournaments);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Crear un nuevo torneo
async function createTournament(req, res) {
  try {
    const { name, type, date } = req.body;
    const tournament = await Tournament.create({ name, type, date });
    res.status(201).json(tournament);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Obtener un torneo por su ID
async function getTournamentById(req, res) {
  try {
    const { id } = req.params;
    const tournament = await Tournament.findByPk(id);
    if (tournament) {
      res.status(200).json(tournament);
    } else {
      res.status(404).json({ error: 'Tournament not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Actualizar un torneo
async function updateTournament(req, res, next) {
  try {
    const { id } = req.params;
    const tournament = await Tournament.findByPk(id);
    if (!tournament) {
      return res.status(404).json({ message: 'Torneo no encontrado' });
    }
    await tournament.update(req.body);
    res.json(tournament);
  } catch (error) {
    next(error);
  }
}

// Eliminar un torneo por su ID
async function deleteTournament(req, res) {
  try {
    const { id } = req.params;
    const tournament = await Tournament.findByPk(id);
    if (tournament) {
      await tournament.destroy();
      res.status(204).end();
    } else {
      res.status(404).json({ error: 'Tournament not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports = {
  getAllTournaments,
  createTournament,
  getTournamentById,
  updateTournament,
  deleteTournament,
};
