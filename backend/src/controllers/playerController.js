const { Player } = require('../models');

// Obtener todos los jugadores
async function getAllPlayers(req, res) {
  try {
    const players = await Player.findAll();
    res.status(200).json(players);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Crear un nuevo jugador
async function createPlayer(req, res) {
  try {
    const { name, skillLevel } = req.body;
    const player = await Player.create({ name, skillLevel });
    res.status(201).json(player);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Obtener un jugador por su ID
async function getPlayerById(req, res) {
  try {
    const { id } = req.params;
    const player = await Player.findByPk(id);
    if (player) {
      res.status(200).json(player);
    } else {
      res.status(404).json({ error: 'Player not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Actualizar un jugador
async function updatePlayer(req, res, next) {
  try {
    const { id } = req.params;
    const player = await Player.findByPk(id);
    if (!player) {
      return res.status(404).json({ message: 'Jugador no encontrado' });
    }
    await player.update(req.body);
    res.json(player);
  } catch (error) {
    next(error);
  }
}

// Eliminar un jugador por su ID
async function deletePlayer(req, res) {
  try {
    const { id } = req.params;
    const player = await Player.findByPk(id);
    if (player) {
      await player.destroy();
      res.status(204).end();
    } else {
      res.status(404).json({ error: 'Player not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

module.exports = {
  getAllPlayers,
  createPlayer,
  getPlayerById,
  updatePlayer,
  deletePlayer,
};
