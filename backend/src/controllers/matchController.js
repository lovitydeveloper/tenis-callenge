const { Match, Player } = require('../models');

// Simular un enfrentamiento
async function simulateMatch(req, res) {
  try {
    const { player1Id, player2Id, tournamentId } = req.body;

    // Obtener jugadores
    const player1 = await Player.findByPk(player1Id);
    const player2 = await Player.findByPk(player2Id);

    // Verificar que los jugadores existan
    if (!player1 || !player2) {
      return res.status(404).json({ error: 'Player not found' });
    }

    // Obtener los parámetros adicionales según el tipo de torneo
    const { type } = await Tournament.findByPk(tournamentId);

    let winner;
    if (type === 'Masculino') {
      const luckLevel = generateLuckLevel();
      winner = determineWinner(player1, player2, luckLevel);
    } else if (type === 'Femenino') {
      const reactionTime = generateReactionTime();
      winner = determineWinner(player1, player2, reactionTime);
    }

    // Crear el registro del enfrentamiento en la base de datos
    const match = await Match.create({
      player1Id,
      player2Id,
      tournamentId,
      winnerId: winner.id,
    });

    res.status(200).json({ match, winner });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Generar un nivel de suerte al azar (0-10)
function generateLuckLevel() {
  return Math.floor(Math.random() * 11);
}

// Generar un tiempo de reacción al azar (en milisegundos)
function generateReactionTime() {
  return Math.floor(Math.random() * 1000);
}

// Determinar al ganador del enfrentamiento basado en los parámetros adicionales
function determineWinner(player1, player2, additionalParameter) {
  // Lógica para determinar al ganador basada en los parámetros adicionales y nivel de habilidad

  // Ejemplo: Comparar niveles de habilidad
  if (player1.skillLevel > player2.skillLevel) {
    return player1;
  } else if (player1.skillLevel < player2.skillLevel) {
    return player2;
  } else {
    // Si los niveles de habilidad son iguales, elige al azar
    const randomIndex = Math.floor(Math.random() * 2);
    return randomIndex === 0 ? player1 : player2;
  }
}

// Obtener partidos por torneo
async function getMatchesByTournament(req, res, next) {
  try {
    const { tournamentId } = req.params;
    const matches = await Match.findAll({ where: { tournamentId } });
    res.json(matches);
  } catch (error) {
    next(error);
  }
}

// Crear un partido
async function createMatch(req, res, next) {
  try {
    const match = await Match.create(req.body);
    res.status(201).json(match);
  } catch (error) {
    next(error);
  }
}

// Actualizar un partido
async function updateMatch(req, res, next) {
  try {
    const { id } = req.params;
    const match = await Match.findByPk(id);
    if (!match) {
      return res.status(404).json({ message: 'Partido no encontrado' });
    }
    await match.update(req.body);
    res.json(match);
  } catch (error) {
    next(error);
  }
}

// Eliminar un partido
async function deleteMatch(req, res, next) {
  try {
    const { id } = req.params;
    const match = await Match.findByPk(id);
    if (!match) {
      return res.status(404).json({ message: 'Partido no encontrado' });
    }
    await match.destroy();
    res.json({ message: 'Partido eliminado correctamente' });
  } catch (error) {
    next(error);
  }
}

module.exports = {
  simulateMatch,
  getMatchesByTournament,
  createMatch,
  updateMatch,
  deleteMatch,
};
