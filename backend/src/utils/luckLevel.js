// Genera un número aleatorio entre 0 y 10 para representar el nivel de suerte
function generateLuckLevel() {
    return Math.floor(Math.random() * 11);
  }
  
  module.exports = {
    generateLuckLevel,
  };
  