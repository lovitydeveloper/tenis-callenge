const { Match } = require('../models');

// Obtener todos los enfrentamientos de un torneo
async function getMatchesByTournamentId(tournamentId) {
  try {
    const matches = await Match.findAll({
      where: { TournamentId: tournamentId },
    });
    return matches;
  } catch (error) {
    throw new Error('Error retrieving matches from the database');
  }
}

// Obtener un enfrentamiento por su ID
async function getMatchById(matchId) {
  try {
    const match = await Match.findByPk(matchId);
    if (!match) {
      throw new Error('Match not found');
    }
    return match;
  } catch (error) {
    throw new Error('Error retrieving match from the database');
  }
}

module.exports = {
  getMatchesByTournamentId,
  getMatchById,
};
