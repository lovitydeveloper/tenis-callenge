const { Tournament, Player, Match } = require('../models');
const luckLevel = require('./luckLevel');

// Obtener todos los torneos
async function getAllTournaments() {
  try {
    const tournaments = await Tournament.findAll();
    return tournaments;
  } catch (error) {
    throw new Error('Error retrieving tournaments from the database');
  }
}

// Obtener un torneo por su ID
async function getTournamentById(tournamentId) {
  try {
    const tournament = await Tournament.findByPk(tournamentId, {
      include: [{ model: Player }],
    });
    if (!tournament) {
      throw new Error('Tournament not found');
    }
    return tournament;
  } catch (error) {
    throw new Error('Error retrieving tournament from the database');
  }
}

// Crear un nuevo torneo
async function createTournament(tournamentData) {
  try {
    const tournament = await Tournament.create(tournamentData);
    return tournament;
  } catch (error) {
    throw new Error('Error creating tournament');
  }
}

// Actualizar un torneo existente
async function updateTournament(tournamentId, tournamentData) {
  try {
    const tournament = await Tournament.findByPk(tournamentId);
    if (!tournament) {
      throw new Error('Tournament not found');
    }
    await tournament.update(tournamentData);
    return tournament;
  } catch (error) {
    throw new Error('Error updating tournament');
  }
}

// Eliminar un torneo existente
async function deleteTournament(tournamentId) {
  try {
    const tournament = await Tournament.findByPk(tournamentId);
    if (!tournament) {
      throw new Error('Tournament not found');
    }
    await tournament.destroy();
  } catch (error) {
    throw new Error('Error deleting tournament');
  }
}

// Simular el torneo y obtener al ganador
async function simulateTournament(tournamentId) {
  try {
    const tournament = await Tournament.findByPk(tournamentId, {
      include: [{ model: Player }],
    });
    if (!tournament) {
      throw new Error('Tournament not found');
    }
    const { Players } = tournament;
    let remainingPlayers = Players;
    
    while (remainingPlayers.length > 1) {
      const winners = [];
      const matches = [];
      
      for (let i = 0; i < remainingPlayers.length; i += 2) {
        const player1 = remainingPlayers[i];
        const player2 = remainingPlayers[i + 1];
        const luck = luckLevel(); // Obtener nivel de suerte al azar
        
        // Simular el enfrentamiento entre los jugadores
        const winner = simulateMatch(player1, player2, luck);
        winners.push(winner);
        
        // Crear el registro del enfrentamiento en la base de datos
        const matchData = {
          Player1Id: player1.id,
          Player2Id: player2.id,
          WinnerId: winner.id,
          LuckLevel: luck,
          TournamentId: tournament.id,
        };
        const match = await Match.create(matchData);
        matches.push(match);
      }
      
      remainingPlayers = winners;
      tournament.Players = remainingPlayers;
      tournament.Matches = matches;
    }
    
    const winner = remainingPlayers[0];
    return winner;
  } catch (error) {
    throw new Error('Error simulating tournament');
  }
}

// Simular un enfrentamiento entre dos jugadores
function simulateMatch(player1, player2, luck) {
  const skillDifference = Math.abs(player1.skill - player2.skill);
  const totalLuck = skillDifference + luck;
  
  if (totalLuck > 10) {
    return player1.skill > player2.skill ? player1 : player2;
  } else {
    const random = Math.random();
    return random < 0.5 ? player1 : player2;
  }
}

module.exports = {
  getAllTournaments,
  getTournamentById,
  createTournament,
  updateTournament,
  deleteTournament,
  simulateTournament,
};
