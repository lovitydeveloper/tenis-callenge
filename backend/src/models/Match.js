const { DataTypes } = require('sequelize');
const { sequelize } = require('../config/database');
const Player = require('./Player');
const Tournament = require('./Tournament');

const Match = sequelize.define('Match', {
  winnerId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
});

Match.belongsTo(Player, { as: 'player1', foreignKey: 'player1Id' });
Match.belongsTo(Player, { as: 'player2', foreignKey: 'player2Id' });
Match.belongsTo(Tournament);

module.exports = Match;
