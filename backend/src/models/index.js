const Match = require('./Match');
const Player = require('./Player');
const Tournament = require('./Tournament');

module.exports = {
  Match,
  Player,
  Tournament,
};
