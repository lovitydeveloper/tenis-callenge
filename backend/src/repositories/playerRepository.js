const { Player } = require('../models');

// Obtener todos los jugadores
async function getAllPlayers() {
  try {
    const players = await Player.findAll();
    return players;
  } catch (error) {
    throw new Error('Error retrieving players from the database');
  }
}

// Obtener un jugador por su ID
async function getPlayerById(playerId) {
  try {
    const player = await Player.findByPk(playerId);
    if (!player) {
      throw new Error('Player not found');
    }
    return player;
  } catch (error) {
    throw new Error('Error retrieving player from the database');
  }
}

// Crear un nuevo jugador
async function createPlayer(playerData) {
  try {
    const player = await Player.create(playerData);
    return player;
  } catch (error) {
    throw new Error('Error creating player');
  }
}

// Actualizar un jugador existente
async function updatePlayer(playerId, playerData) {
  try {
    const player = await Player.findByPk(playerId);
    if (!player) {
      throw new Error('Player not found');
    }
    await player.update(playerData);
    return player;
  } catch (error) {
    throw new Error('Error updating player');
  }
}

// Eliminar un jugador existente
async function deletePlayer(playerId) {
  try {
    const player = await Player.findByPk(playerId);
    if (!player) {
      throw new Error('Player not found');
    }
    await player.destroy();
  } catch (error) {
    throw new Error('Error deleting player');
  }
}

module.exports = {
  getAllPlayers,
  getPlayerById,
  createPlayer,
  updatePlayer,
  deletePlayer,
};
