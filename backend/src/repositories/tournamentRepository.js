const { Tournament } = require('../models');

// Obtener todos los torneos
async function getAllTournaments() {
  try {
    const tournaments = await Tournament.findAll();
    return tournaments;
  } catch (error) {
    throw new Error('Error retrieving tournaments from the database');
  }
}

// Obtener un torneo por su ID
async function getTournamentById(tournamentId) {
  try {
    const tournament = await Tournament.findByPk(tournamentId);
    if (!tournament) {
      throw new Error('Tournament not found');
    }
    return tournament;
  } catch (error) {
    throw new Error('Error retrieving tournament from the database');
  }
}

// Crear un nuevo torneo
async function createTournament(tournamentData) {
  try {
    const tournament = await Tournament.create(tournamentData);
    return tournament;
  } catch (error) {
    throw new Error('Error creating tournament');
  }
}

// Actualizar un torneo existente
async function updateTournament(tournamentId, tournamentData) {
  try {
    const tournament = await Tournament.findByPk(tournamentId);
    if (!tournament) {
      throw new Error('Tournament not found');
    }
    await tournament.update(tournamentData);
    return tournament;
  } catch (error) {
    throw new Error('Error updating tournament');
  }
}

// Eliminar un torneo existente
async function deleteTournament(tournamentId) {
  try {
    const tournament = await Tournament.findByPk(tournamentId);
    if (!tournament) {
      throw new Error('Tournament not found');
    }
    await tournament.destroy();
  } catch (error) {
    throw new Error('Error deleting tournament');
  }
}

module.exports = {
  getAllTournaments,
  getTournamentById,
  createTournament,
  updateTournament,
  deleteTournament,
};
