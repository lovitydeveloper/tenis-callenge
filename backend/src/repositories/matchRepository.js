const { Match } = require('../models');

// Obtener todos los enfrentamientos de un torneo
async function getMatchesByTournament(tournamentId) {
  try {
    const matches = await Match.findAll({
      where: {
        TournamentId: tournamentId,
      },
    });
    return matches;
  } catch (error) {
    throw new Error('Error retrieving matches from the database');
  }
}

// Crear un nuevo enfrentamiento
async function createMatch(matchData) {
  try {
    const match = await Match.create(matchData);
    return match;
  } catch (error) {
    throw new Error('Error creating match');
  }
}

// Actualizar un enfrentamiento existente
async function updateMatch(matchId, matchData) {
  try {
    const match = await Match.findByPk(matchId);
    if (!match) {
      throw new Error('Match not found');
    }
    await match.update(matchData);
    return match;
  } catch (error) {
    throw new Error('Error updating match');
  }
}

// Eliminar un enfrentamiento existente
async function deleteMatch(matchId) {
  try {
    const match = await Match.findByPk(matchId);
    if (!match) {
      throw new Error('Match not found');
    }
    await match.destroy();
  } catch (error) {
    throw new Error('Error deleting match');
  }
}

module.exports = {
  getMatchesByTournament,
  createMatch,
  updateMatch,
  deleteMatch,
};
