const { Sequelize } = require('sequelize');
require('dotenv').config()

// Configuración de la conexión a la base de datos MySQL
const sequelize = new Sequelize({
    //dialect: 'mysql',
    dialect: 'mariadb',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
});


// Función para probar la conexión a la base de datos
async function testConnection() {
    try {
        await sequelize.authenticate();
        console.log('Connection to the database has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

async function db() {
    try {
        await sequelize.authenticate();
        console.log('Connection to the database has been established successfully.');
        return sequelize;
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

module.exports = {
    sequelize: sequelize,
    db,
    testConnection,
};
