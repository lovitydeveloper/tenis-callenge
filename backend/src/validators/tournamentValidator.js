const Joi = require('joi');

// Esquema de validación para el torneo
const tournamentSchema = Joi.object({
  name: Joi.string().required(),
  gender: Joi.string().valid('Male', 'Female').required(),
  players: Joi.array()
    .items(
      Joi.object({
        name: Joi.string().required(),
        skill: Joi.number().min(0).max(100).required(),
      })
    )
    .min(2)
    .required(),
});

// Validar los datos del torneo
function validateTournament(data) {
  return tournamentSchema.validate(data);
}

module.exports = {
  validateTournament,
};
