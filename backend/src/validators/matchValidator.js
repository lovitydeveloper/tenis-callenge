const Joi = require('joi');

// Esquema de validación para el enfrentamiento
const matchSchema = Joi.object({
  tournamentId: Joi.number().required(),
  player1Id: Joi.number().required(),
  player2Id: Joi.number().required(),
  result: Joi.string().valid('Player1', 'Player2').required(),
  luckLevel: Joi.number().min(0).max(10).required(),
});

// Validar los datos del enfrentamiento
function validateMatch(data) {
  return matchSchema.validate(data);
}

module.exports = {
  validateMatch,
};
