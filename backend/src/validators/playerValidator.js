const Joi = require('joi');

// Esquema de validación para el jugador
const playerSchema = Joi.object({
  name: Joi.string().required(),
  skill: Joi.number().min(0).max(100).required(),
});

// Validar los datos del jugador
function validatePlayer(data) {
  return playerSchema.validate(data);
}

module.exports = {
  validatePlayer,
};
