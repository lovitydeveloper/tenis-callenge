const express = require('express');
const router = express.Router();
const tournamentController = require('../controllers/tournamentController');

// Endpoint para obtener todos los torneos
router.get('/', tournamentController.getAllTournaments);

// Endpoint para obtener un torneo por su ID
router.get('/:id', tournamentController.getTournamentById);

// Endpoint para crear un nuevo torneo
router.post('/', tournamentController.createTournament);

// Endpoint para actualizar un torneo existente
router.put('/:id', tournamentController.updateTournament);

// Endpoint para eliminar un torneo existente
router.delete('/:id', tournamentController.deleteTournament);

module.exports = router;
