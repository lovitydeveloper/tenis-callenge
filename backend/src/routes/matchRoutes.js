const express = require('express');
const router = express.Router();
const matchController = require('../controllers/matchController');

// Endpoint para obtener todos los enfrentamientos de un torneo
router.get('/tournament/:tournamentId', matchController.getMatchesByTournament);

// Endpoint para crear un nuevo enfrentamiento
router.post('/', matchController.createMatch);

// Endpoint para actualizar un enfrentamiento existente
router.put('/:id', matchController.updateMatch);

// Endpoint para eliminar un enfrentamiento existente
router.delete('/:id', matchController.deleteMatch);

module.exports = router;
