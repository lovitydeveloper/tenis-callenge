const express = require('express');
const router = express.Router();
const playerController = require('../controllers/playerController');

// Endpoint para obtener todos los jugadores
router.get('/', playerController.getAllPlayers);

// Endpoint para obtener un jugador por su ID
router.get('/:id', playerController.getPlayerById);

// Endpoint para crear un nuevo jugador
router.post('/', playerController.createPlayer);

// Endpoint para actualizar un jugador existente
router.put('/:id', playerController.updatePlayer);

// Endpoint para eliminar un jugador existente
router.delete('/:id', playerController.deletePlayer);

module.exports = router;
