const Match = require('./models/Match');
const { matches } = require('./create_matches');

// Función para crear los partidos en la base de datos
async function seedMatches() {
  try {
    // Crear los partidos en la base de datos
    await Match.bulkCreate(matches);

    console.log('Matches seeded successfully');
  } catch (error) {
    console.error('Error seeding matches:', error);
  }
}

// Ejecutar la función para poblar la base de datos
seedMatches();
