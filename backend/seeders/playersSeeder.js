const Player = require('./models/Player');
const { malePlayers, femalePlayers } = require('./create_players');

// Función para crear los jugadores en la base de datos
async function seedPlayers() {
  try {
    // Crear los jugadores en la base de datos
    const createdMalePlayers = await Player.bulkCreate(malePlayers, {
      individualHooks: true,
    });

    const createdFemalePlayers = await Player.bulkCreate(femalePlayers, {
      individualHooks: true,
    });

    console.log('Players seeded successfully');
  } catch (error) {
    console.error('Error seeding players:', error);
  }
}

// Ejecutar la función para poblar la base de datos
seedPlayers();
