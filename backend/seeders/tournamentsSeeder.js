const Tournament = require('./models/Tournament');
const Player = require('./models/Player');
const { maleTournament, femaleTournament } = require('./create_tournaments');

// Función para crear los torneos y jugadores en la base de datos
async function seedTournaments() {
  try {
    // Crear los torneos en la base de datos
    const createdMaleTournament = await Tournament.create(maleTournament);
    const createdFemaleTournament = await Tournament.create(femaleTournament);

    // Crear los jugadores en la base de datos
    const malePlayers = maleTournament.getPlayers();
    const femalePlayers = femaleTournament.getPlayers();

    const createdMalePlayers = await Player.bulkCreate(malePlayers, {
      individualHooks: true,
    });

    const createdFemalePlayers = await Player.bulkCreate(femalePlayers, {
      individualHooks: true,
    });

    // Asociar los jugadores a los torneos
    await createdMaleTournament.addPlayers(createdMalePlayers);
    await createdFemaleTournament.addPlayers(createdFemalePlayers);

    console.log('Tournaments seeded successfully');
  } catch (error) {
    console.error('Error seeding tournaments:', error);
  }
}

// Ejecutar la función para poblar la base de datos
seedTournaments();
