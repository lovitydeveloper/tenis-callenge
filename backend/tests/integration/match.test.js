const request = require('supertest');
const app = require('../app');
const db = require('../config/database');

// Antes de ejecutar las pruebas, asegurarse de sincronizar los modelos con la base de datos
beforeAll(async () => {
  await db.sync();
});

// Después de ejecutar las pruebas, cerrar la conexión a la base de datos
afterAll(async () => {
  await db.close();
});

describe('Match API', () => {
  it('should create a new match', async () => {
    const response = await request(app)
      .post('/api/matches')
      .send({
        player1Id: 1,
        player2Id: 2,
        tournamentId: 1,
      });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.player1Id).toBe(1);
    expect(response.body.player2Id).toBe(2);
    expect(response.body.tournamentId).toBe(1);
  });

  it('should retrieve a match by ID', async () => {
    const createdMatch = await request(app)
      .post('/api/matches')
      .send({
        player1Id: 1,
        player2Id: 2,
        tournamentId: 1,
      });

    const response = await request(app).get(`/api/matches/${createdMatch.body.id}`);

    expect(response.status).toBe(200);
    expect(response.body.player1Id).toBe(1);
    expect(response.body.player2Id).toBe(2);
    expect(response.body.tournamentId).toBe(1);
  });

  it('should retrieve all matches', async () => {
    const response = await request(app).get('/api/matches');

    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true);
  });

  it('should update a match', async () => {
    const createdMatch = await request(app)
      .post('/api/matches')
      .send({
        player1Id: 1,
        player2Id: 2,
        tournamentId: 1,
      });

    const response = await request(app)
      .put(`/api/matches/${createdMatch.body.id}`)
      .send({ player1Score: 6, player2Score: 4 });

    expect(response.status).toBe(200);
    expect(response.body.player1Score).toBe(6);
    expect(response.body.player2Score).toBe(4);
  });

  it('should delete a match', async () => {
    const createdMatch = await request(app)
      .post('/api/matches')
      .send({
        player1Id: 1,
        player2Id: 2,
        tournamentId: 1,
      });

    const response = await request(app).delete(`/api/matches/${createdMatch.body.id}`);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe('Match deleted');
  });
});
