const request = require('supertest');
const app = require('../app');
const db = require('../config/database');

// Antes de ejecutar las pruebas, asegurarse de sincronizar los modelos con la base de datos
beforeAll(async () => {
  await db.sync();
});

// Después de ejecutar las pruebas, cerrar la conexión a la base de datos
afterAll(async () => {
  await db.close();
});

describe('Player API', () => {
  it('should create a new player', async () => {
    const response = await request(app)
      .post('/api/players')
      .send({
        name: 'John Doe',
        skill: 85,
      });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.name).toBe('John Doe');
  });

  it('should retrieve a player by ID', async () => {
    const createdPlayer = await request(app)
      .post('/api/players')
      .send({
        name: 'John Doe',
        skill: 85,
      });

    const response = await request(app).get(`/api/players/${createdPlayer.body.id}`);

    expect(response.status).toBe(200);
    expect(response.body.name).toBe('John Doe');
  });

  it('should retrieve all players', async () => {
    const response = await request(app).get('/api/players');

    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true);
  });

  it('should update a player', async () => {
    const createdPlayer = await request(app)
      .post('/api/players')
      .send({
        name: 'John Doe',
        skill: 85,
      });

    const response = await request(app)
      .put(`/api/players/${createdPlayer.body.id}`)
      .send({ name: 'Updated Player' });

    expect(response.status).toBe(200);
    expect(response.body.name).toBe('Updated Player');
  });

  it('should delete a player', async () => {
    const createdPlayer = await request(app)
      .post('/api/players')
      .send({
        name: 'John Doe',
        skill: 85,
      });

    const response = await request(app).delete(`/api/players/${createdPlayer.body.id}`);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe('Player deleted');
  });
});
