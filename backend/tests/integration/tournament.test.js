const request = require('supertest');
const app = require('../app');
const db = require('../config/database');

// Antes de ejecutar las pruebas, asegurarse de sincronizar los modelos con la base de datos
beforeAll(async () => {
  await db.sync();
});

// Después de ejecutar las pruebas, cerrar la conexión a la base de datos
afterAll(async () => {
  await db.close();
});

describe('Tournament API', () => {
  it('should create a new tournament', async () => {
    const response = await request(app)
      .post('/api/tournaments')
      .send({
        name: 'Tennis Tournament',
        gender: 'Male',
        players: [
          { name: 'Player 1', skill: 80 },
          { name: 'Player 2', skill: 90 },
          { name: 'Player 3', skill: 70 },
          { name: 'Player 4', skill: 85 },
        ],
      });

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('id');
    expect(response.body.name).toBe('Tennis Tournament');
  });

  it('should retrieve a tournament by ID', async () => {
    const createdTournament = await request(app)
      .post('/api/tournaments')
      .send({
        name: 'Tennis Tournament',
        gender: 'Male',
        players: [
          { name: 'Player 1', skill: 80 },
          { name: 'Player 2', skill: 90 },
          { name: 'Player 3', skill: 70 },
          { name: 'Player 4', skill: 85 },
        ],
      });

    const response = await request(app).get(`/api/tournaments/${createdTournament.body.id}`);

    expect(response.status).toBe(200);
    expect(response.body.name).toBe('Tennis Tournament');
  });

  it('should retrieve all tournaments', async () => {
    const response = await request(app).get('/api/tournaments');

    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true);
  });

  it('should update a tournament', async () => {
    const createdTournament = await request(app)
      .post('/api/tournaments')
      .send({
        name: 'Tennis Tournament',
        gender: 'Male',
        players: [
          { name: 'Player 1', skill: 80 },
          { name: 'Player 2', skill: 90 },
          { name: 'Player 3', skill: 70 },
          { name: 'Player 4', skill: 85 },
        ],
      });

    const response = await request(app)
      .put(`/api/tournaments/${createdTournament.body.id}`)
      .send({ name: 'Updated Tournament' });

    expect(response.status).toBe(200);
    expect(response.body.name).toBe('Updated Tournament');
  });

  it('should delete a tournament', async () => {
    const createdTournament = await request(app)
      .post('/api/tournaments')
      .send({
        name: 'Tennis Tournament',
        gender: 'Male',
        players: [
          { name: 'Player 1', skill: 80 },
          { name: 'Player 2', skill: 90 },
          { name: 'Player 3', skill: 70 },
          { name: 'Player 4', skill: 85 },
        ],
      });

    const response = await request(app).delete(`/api/tournaments/${createdTournament.body.id}`);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe('Tournament deleted');
  });
});
