const Tournament = require('../Tournament');

describe('Tournament', () => {
  it('should initialize with correct properties', () => {
    const tournament = new Tournament('My Tournament', 'Male');

    expect(tournament.name).toBe('My Tournament');
    expect(tournament.type).toBe('Male');
    expect(tournament.players).toEqual([]);
    expect(tournament.winner).toBeNull();
    expect(tournament.rounds).toEqual([]);
  });

  it('should add players correctly', () => {
    const tournament = new Tournament('My Tournament', 'Male');
    const player1 = { name: 'Player 1', skill: 80 };
    const player2 = { name: 'Player 2', skill: 90 };

    tournament.addPlayer(player1);
    tournament.addPlayer(player2);

    expect(tournament.players.length).toBe(2);
    expect(tournament.players[0]).toBe(player1);
    expect(tournament.players[1]).toBe(player2);
  });

  it('should simulate the tournament and determine the winner', () => {
    const tournament = new Tournament('My Tournament', 'Male');
    const player1 = { name: 'Player 1', skill: 80 };
    const player2 = { name: 'Player 2', skill: 90 };
    const player3 = { name: 'Player 3', skill: 85 };
    const player4 = { name: 'Player 4', skill: 95 };

    tournament.addPlayer(player1);
    tournament.addPlayer(player2);
    tournament.addPlayer(player3);
    tournament.addPlayer(player4);

    tournament.simulateTournament();

    expect(tournament.winner).not.toBeNull();
    expect(tournament.winner.skill).toBe(95);
  });
});
