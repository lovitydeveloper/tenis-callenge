const Match = require('../Match');
const Player = require('../Player');

describe('Match', () => {
  let player1;
  let player2;
  let match;

  beforeEach(() => {
    player1 = new Player('John Doe', 80);
    player2 = new Player('Jane Smith', 90);
    match = new Match(player1, player2);
  });

  it('should initialize with correct properties', () => {
    expect(match.player1).toBe(player1);
    expect(match.player2).toBe(player2);
    expect(match.winner).toBeNull();
    expect(match.luckLevel).toBeGreaterThanOrEqual(0);
    expect(match.luckLevel).toBeLessThanOrEqual(10);
  });

  it('should determine the winner based on skill and luck level', () => {
    match.play();

    expect(match.winner).toBeDefined();
    expect([player1, player2]).toContain(match.winner);
  });
});
