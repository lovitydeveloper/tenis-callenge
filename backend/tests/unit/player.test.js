const Player = require('../Player');

describe('Player', () => {
  it('should initialize with correct properties', () => {
    const player = new Player('John Doe', 85);

    expect(player.name).toBe('John Doe');
    expect(player.skill).toBe(85);
  });

  it('should calculate luck level correctly', () => {
    const player = new Player('John Doe', 85);

    const luckLevel = player.luckLevel();

    expect(luckLevel).toBeGreaterThanOrEqual(0);
    expect(luckLevel).toBeLessThanOrEqual(10);
  });
});
