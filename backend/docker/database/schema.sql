-- Crear la tabla de jugadores
CREATE TABLE players (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  skill_level INT NOT NULL,
  gender ENUM('Male', 'Female') NOT NULL
);

-- Crear la tabla de partidos
CREATE TABLE matches (
  id INT AUTO_INCREMENT PRIMARY KEY,
  player1_id INT NOT NULL,
  player2_id INT NOT NULL,
  tournament_id INT NOT NULL,
  winner_id INT,
  luck_level INT NOT NULL,
  FOREIGN KEY (player1_id) REFERENCES players(id),
  FOREIGN KEY (player2_id) REFERENCES players(id),
  FOREIGN KEY (tournament_id) REFERENCES tournaments(id),
  FOREIGN KEY (winner_id) REFERENCES players(id)
);

-- Crear la tabla de torneos
CREATE TABLE tournaments (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  type ENUM('Male', 'Female') NOT NULL,
  start_date DATE NOT NULL
);
