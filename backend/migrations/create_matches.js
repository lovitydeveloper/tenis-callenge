const Match = require('./Match');
const { malePlayers, femalePlayers } = require('./create_players');

// Crear enfrentamientos para el torneo masculino
const maleMatches = [];
for (let i = 0; i < malePlayers.length; i += 2) {
  const player1 = malePlayers[i];
  const player2 = malePlayers[i + 1];
  const match = new Match(player1, player2);
  maleMatches.push(match);
}

// Crear enfrentamientos para el torneo femenino
const femaleMatches = [];
for (let i = 0; i < femalePlayers.length; i += 2) {
  const player1 = femalePlayers[i];
  const player2 = femalePlayers[i + 1];
  const match = new Match(player1, player2);
  femaleMatches.push(match);
}

// Exportar los enfrentamientos creados
module.exports = {
  maleMatches,
  femaleMatches,
};
