const Player = require('./Player');

// Crear jugadores masculinos
const malePlayers = [
  new Player('Player 1', 80),
  new Player('Player 2', 85),
  new Player('Player 3', 90),
  new Player('Player 4', 95),
];

// Crear jugadores femeninos
const femalePlayers = [
  new Player('Player A', 75),
  new Player('Player B', 85),
  new Player('Player C', 90),
  new Player('Player D', 92),
];

// Exportar los jugadores creados
module.exports = {
  malePlayers,
  femalePlayers,
};
