const Tournament = require('./Tournament');
const Player = require('./Player');

// Crear torneos
const maleTournament = new Tournament('Male Tournament', 'Male');
const femaleTournament = new Tournament('Female Tournament', 'Female');

// Crear jugadores
const malePlayers = [
  new Player('Player 1', 80),
  new Player('Player 2', 85),
  new Player('Player 3', 90),
  new Player('Player 4', 95),
];

const femalePlayers = [
  new Player('Player A', 75),
  new Player('Player B', 85),
  new Player('Player C', 90),
  new Player('Player D', 92),
];

// Agregar jugadores a los torneos
malePlayers.forEach((player) => maleTournament.addPlayer(player));
femalePlayers.forEach((player) => femaleTournament.addPlayer(player));

// Exportar los torneos creados
module.exports = {
  maleTournament,
  femaleTournament,
};
